<?php
/* 
  * * @file        /inc/loader.php
  * * @project     GecK0ntent
  * * @author      GeCk0 <mail@linksgelenkt.de>
  * * @copyright   2015 linksgelenkt.de
  * * @license     license.txt CC-by-SA 4.0
  * * @todo        
 */

debug ('#############################');
debug ('starting loader.php - actions');

## Funktionen Laden
if (is_dir('./inc/func/')) {    
    $dir_func = scandir('./inc/func/');
    foreach ($dir_func as $i) {
        if ($i != '.' && $i != '..'){
            
           $filetyp = explode('.',$i);
           
            // prüfen ob es eine *.func.php ist
            if ($filetyp[1] == 'func' && $filetyp[2] == 'php') {
                debug('  loading ./inc/func/'.$i);
                $l = include_once('./inc/func/'.$i);
                if(!isset($i) OR $i === FALSE) {
                    debug('  Error loading ./inc/func/'.$i);
                }
            }
        }
    }
} else {
    debug('  ./inc/func/ NICHT GEFUNDEN oder NICHT LESBAR');
}

## Klassen laden

if (is_dir('./inc/class/')) {    
    $dir_func = scandir('./inc/class/');
    foreach ($dir_func as $i) {
        if ($i != '.' && $i != '..'){
            
           $filetyp = explode('.',$i);
           
            // prüfen ob es eine *.func.php ist
            if ($filetyp[1] == 'func' && $filetyp[2] == 'php') {
                debug('  loading ./inc/class/'.$i);
                $l = include_once('./inc/class/'.$i);
                if(!isset($i) OR $i === FALSE) {
                    debug('  Error loading ./inc/class/'.$i);
                }
            }
        }
    }
} else {
    debug('  ./inc/class/ NICHT GEFUNDEN oder NICHT LESBAR');
}


debug ('finishing loaded loadable loadings');
debug ('#############################');