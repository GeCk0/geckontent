<?php
/* 
  * * @file        /inc/debug.php
  * * @project     GecK0ntent
  * * @author      GeCk0 <mail@linksgelenkt.de>
  * * @copyright   2015 linksgelenkt.de
  * * @license     license.txt CC-by-SA 4.0
  * * @todo        
 */
/* 
 * @author      GeCk0
 * @name 	dump
 * @param	mixed $var
 * @return 	FALSE on error and outputs a dump
 *  U N F E R T I G
 */
 function dump ($var,$bool = 0) { // $bool ist noch ohne Funktion
	 $output = '</br>';
	 if (is_bool($var) && $var === FALSE) {
		 $output .= '<b>BOOL</b> true</br>';
	 }
         if (is_bool($var) && $var === TRUE){
             $output .= '<b>BOOL</b> false</br>';
        }
	 if (is_string($var)) {
		 $output .= '<b>STRING</b> '.$var.'</br>';
	 }
	 if (is_array($var)) {
		 foreach ($var as $key) {
                    $output .= ' => '.print_r($key).'</br>';
		} 
	 } 
	 if (is_int($var)) {
		 $output .= '<b>INT</b> '.$var.'</br>';
	 }
        
	echo '<pre>'.$output.'</pre></br>';
 }
/*
 * @author      GeCk0
 * @name: 	debug
 * @param	mixed
 * @return 	übergibt eine Info an die Fehlerausgabe, falls debugmode in der index.php eingeschaltet wurde
 * 
 */
function debug ($string,$bool=0) {
	if (is_string($string)) {
		$_SESSION['debug']['output'][] = $string.'<br>';
	} 
	if ($bool == 1) {
		$output = '';
		foreach ($_SESSION['debug']['output'] as $debug_msg) {
			$output .= $debug_msg.'</br>';
		}
		print_r ('<div class="debug"><pre>'.$output.'</pre></div>');
                $_SESSION['debug']['output'] = FALSE;
		#var_dump($_SESSION['debug']['output']);
	} 
}

