<?php
/* 
  * * @file        /inc/func/tpl.func.php
  * * @project     GecK0ntent
  * * @author      GeCk0 <mail@linksgelenkt.de>
  * * @copyright   2015 linksgelenkt.de
  * * @license     license.txt CC-by-SA 4.0
  * * @todo        
 */

/*
 * @author      GeCk0
 * @name	template
 * @param	
 * @return 	a string, contains the html-code to be showed in browser
 * 
 */

function template($bool = 0) {
    global $opt_tpl;
    global $opt_title;
    global $opt_sitename;
    
    // check if all files are available
    if (is_file('./inc/tpl/head.htm') &&
        is_file('./inc/tpl/'.$opt_tpl.'.htm') &&
        is_file('./inc/tpl/'.$opt_tpl.'.css') &&
        is_file('./inc/tpl/footer.htm')) {
            // create the content
            $header  = file_get_contents('./inc/tpl/head.htm');
            $content = file_get_contents('./inc/tpl/'.$opt_tpl.'.htm');
            $footer  = file_get_contents('./inc/tpl/footer.htm');
            $style   = './inc/tpl/'.$opt_tpl.'.css';
            
            // template replaces
            $header1 = str_replace('{TITLE}', $opt_title, $header);
            $header2 = str_replace('{STYLE}', $style, $header1);
           
            ## @todo    use a $opt_title.config of the tpl to get replace-rulez
            $content1 = str_replace('{CONTENT}', 'Ausgabe von &_GET["c"]', $content);
            $content2 = str_replace('{TITLE}', $opt_title, $content1);
            $content3 = str_replace('{SITENAME}', $opt_sitename, $content2);
            
            $context = $header2.$content3.$footer;

            // return if $bool = 1
            if (isset($bool) && $bool != 0) {        
                echo $context;
            } else {
                return $context;
            }
            
    } else {
        debug ('<b> ERROR:</b> Konnte Template nicht laden (head.htm, footer.htm, '.$opt_tpl.'.css und '.$opt_tpl.'.htm)');
    }
}