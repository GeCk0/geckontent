<?php
/* 
  * * @file        /inc/config.php
  * * @project     GecK0ntent
  * * @author      GeCk0 <mail@linksgelenkt.de>
  * * @copyright   2015 linksgelenkt.de
  * * @license     license.txt CC-by-SA 4.0
  * * @todo        put settings into mysql and use this file only for sql-connect
 */
$opt_debug  = 0; //Debug-Mode aktivieren ? ja = 1; nein = 0;
$opt_tpl = 'geck0tent'; // Name der Templatedatei in /inc/tpl ohne.htm
$opt_title = 'lang isser nicht... aber dünn...';
$opt_sitename = 'GeCkOntent';