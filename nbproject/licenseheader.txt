<#if licenseFirst??>${licenseFirst}</#if>
 ${licensePrefix}* @file        FILENAME.php
 ${licensePrefix}* @project     GecK0ntent
 ${licensePrefix}* @author      GeCk0 <mail@linksgelenkt.de>
 ${licensePrefix}* @copyright   2015 linksgelenkt.de
 ${licensePrefix}* @license     license.txt CC-by-SA 4.0
 ${licensePrefix}* @todo        
<#if licenseLast??>${licenseLast}</#if>