<?php
/* 
  * * @file        index.php
  * * @project     GecK0ntent
  * * @author      GeCk0 <mail@linksgelenkt.de>
  * * @copyright   2015 linksgelenkt.de
  * * @license     license.txt CC-by-SA 4.0
  * * @todo        
 */
  
    $session = session_start();
    if ($session === FALSE) {
        debug('FEHLER beim Laden/Erzeugen der Session');
    } else {
        $sid = session_id();
    }
    
    $check_config = require_once('./inc/config.php'); // @todo in eine Datenbank auslagern ???
    $check_debug = require_once('./inc/debug.php'); //stellt u.a. die Funktion debug() und dump() zur verfügung
    $check_load  = require_once('./inc/loader.php'); //lädt alle PHP-Dateien in /inc/func|class|boxes/
    
    ## config test
    if (isset($opt_tpl) && $opt_tpl !== FALSE) {
        debug('CONFIG: variable gelesen');
    } else {
        debug('CONFIG: Fehler beim Lesen der Variablen');
    }
    
    if (isset($opt_debug)) {
        debug ('debugmodus aktiv');
        
        error_reporting(E_ALL);
        ini_set("display_errors", "on");
        ini_set("display_startip_errors", "on");
        
        if (!isset($check_debug)) { 
            debug('Error loading /inc/func/debug.php');
        } else {
            debug('loaded /inc/func/debug.php ok');
        }
        
        if (!isset($check_load)) { 
            debug('Error loading /inc/loader.php');
        } else {
            debug('loaded /inc/loader.php ok');
        }
        
        if (!isset($check_config)) { 
            debug('Error loading /inc/config.php');
        } else {
            debug('loaded /inc/config.php ok');
        }       
    }
    
    // Das Template ausgeben
    template(1);
    
    // Ausgabe der Debugmeldungen
    debug(
        round(microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"],5).
        ' seconds to load this page'
    );
    if ($opt_debug == 1) {
        debug(0,1);
    }